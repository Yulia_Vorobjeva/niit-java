/**
 * Created by yuliavorobjeva on 05.03.17.
 */
public class Collatz {

    static int N =1;
    static int count = 0;
    static long result = 0;
    static long temp = 0;
    static int i = 0;

    public static long collatz(long n) {
        if (n == 1) return 1;
        else if (n % 2 == 0) return(1+ collatz(n / 2));
        else return(1+ collatz(3 * n + 1));
    }

    public static void main(String[] args) {
        int k = 0;
        for (i = N; i <= 1000000; i++) {
            temp = collatz(i);
            if (temp > result) {
                result = temp;
                k = i;
            }
        }
        System.out.println(result);
    }
}