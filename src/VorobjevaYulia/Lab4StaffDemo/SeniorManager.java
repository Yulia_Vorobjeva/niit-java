/**
 * Created by yuliavorobjeva on 11.04.17.
 */
public class SeniorManager extends ProjectManager {

    @Override
    public double getProjectPayment(){
        return this.rate * this.part * mProjectTypes.size();
    }

    @Override
    protected void calculateTotalPayment(){
        totalPayment = getProjectPayment()+ getHeadingPayment();
    }
}
