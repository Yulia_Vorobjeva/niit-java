/**
 * Created by yuliavorobjeva on 11.04.17.
 */

import java.util.List;


public abstract class Employee{
    protected String fio;
    protected int id;
    protected int worktime = 40;//рабочее время
    protected int payment;//cтавка в час
    protected double totalPayment;//зарплата
    protected int mSubordinatesCount;
    private EmployeeType mEmployeeType;

    public enum  EmployeeType {
        PROGRAMMER("программист"),
        TESTER("тестер"),
        TEAMLEAD("тимлид"),
        PROJECT_MANAGER("менеджер проекта"),
        SENIOR_MANAGER("старший менеджер"),
        DRIVER("водитель"),
        CLEANER("уборщица");

        private final String str;

        EmployeeType(String str){
            this.str = str;
        }

        public static EmployeeType fromString(String typeAsString) {
            for (EmployeeType employeeType : EmployeeType.values()) {
                if (employeeType.str.equalsIgnoreCase(typeAsString)) {
                    return employeeType;
                }
            }
            return null;
        }
    }


    public static class EmployeeFactory {
        public static Employee getEmployeeForType(EmployeeType employeeType) {
            Employee employee;
            switch (employeeType) {
                case CLEANER:
                    employee = new Cleaner();
                    break;
                case DRIVER:
                    employee = new Driver();
                    break;
                case PROGRAMMER:
                    employee = new Programmer();
                    break;
                case PROJECT_MANAGER:
                    employee = new ProjectManager();
                    break;
                case SENIOR_MANAGER:
                    employee = new Programmer();
                    break;
                case TEAMLEAD:
                    employee = new TeamLead();
                    break;
                case TESTER:
                    employee = new Tester();
                    break;
                default:
                    return null;
            }
            employee.setEmployeeType(employeeType);
            return employee;
        }
    }
    private void setEmployeeType(EmployeeType type){
        this.mEmployeeType = type;
    }

    public EmployeeType getEmployeeType(){
        return mEmployeeType;
    }

    public void printInfo() {
        System.out.print("Post: " + mEmployeeType.toString() + ",  ");
        //System.out.print("Id: " + id);
        System.out.print("Name: " + fio + ",   ");
        System.out.print("Payment for a week(40 hours): " + totalPayment + ",  ");

    }

    public abstract void fillWithAdditionalData(List<String> data);


    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.fio = name;
    }

    public void calculatePayment(int subordinatesCount) {
        mSubordinatesCount = subordinatesCount;
        calculateTotalPayment();
    }

    protected abstract void calculateTotalPayment();

    public void setPayment(int ppayment){
        payment = ppayment;
    }
}




