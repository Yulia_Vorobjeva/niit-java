/**
 * Created by yuliavorobjeva on 20.04.17.
 */
public enum ProjectType {
    SAMSUNG("SAMSUNG"),
    YOTA("YOTA"),
    ASKOM("ASKOM");

    private final String mValue;

    ProjectType(String value) {
        mValue = value;
    }

    public static boolean contains(String value) {
        for (ProjectType projectType : ProjectType.values()) {
            if (projectType.name().equals(value)) {
                return true;
            }
        }
        return false;
    }

    public static ProjectType fromString(String typeAsString) {
        for (ProjectType projectType : ProjectType.values()) {
            if (projectType.mValue.equalsIgnoreCase(typeAsString)) {
                return projectType;
            }
        }
        return null;
    }
}