/**
 * Created by yuliavorobjeva on 11.04.17.
 */

import java.util.List;
import java.util.ArrayList;
public  abstract class Manager extends Employee implements Project, Heading{

    protected int rate;//ставка по проекту
    protected double part;//"доля" участия
    protected List<ProjectType> mProjectTypes;

    @Override
    public double getProjectPayment() {
        return rate*part;
    }


    @Override
    public void fillWithAdditionalData(List<String> data) {
        mProjectTypes = new ArrayList<>();
        for (String item : data) {
            if (ProjectType.contains(item)) {
                mProjectTypes.add(ProjectType.valueOf(item));
            } else {
                part = Double.parseDouble(item);
            }
        }
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.print("Projects: ");
        for (ProjectType projectType : mProjectTypes) {
            System.out.print(projectType + ",  ");
        }
        //System.out.println("Part: " + part);
        System.out.println();
    }

    public List<ProjectType> getProjectTypes(){
        return mProjectTypes;
    }
}
