package com.vorobjeva.apps.automat.statemachine;

public interface OnStateChangedListener {
    void onStateChanged(AutomatStateMachine.State state);
}
