package com.vorobjeva.apps.automat.adapters;

import android.widget.Button;

/**
 * Created by yuliavorobjeva on 28.04.17.
 */

public interface OnAutomatButtonClickListener {
    void onAutomatButtonClick(Button button, int price);
}
