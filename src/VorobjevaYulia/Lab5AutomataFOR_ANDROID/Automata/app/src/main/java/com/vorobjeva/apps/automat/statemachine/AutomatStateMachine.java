package com.vorobjeva.apps.automat.statemachine;

public class AutomatStateMachine {

    public enum State {
        READY,
        COINS_INPUT,
        WORKING,
        COFFEE,
        CHANGE,
        ERROR
    }

    private State mState;
    private OnStateChangedListener mStateChangedListener;

    public AutomatStateMachine(OnStateChangedListener stateChangedListener) {
        mStateChangedListener = stateChangedListener;
        mState = State.READY;
        if (null != mStateChangedListener) {
            mStateChangedListener.onStateChanged(mState);
        }
    }

    public void reset() {
        mState = State.READY;
        if (null != mStateChangedListener) {
            mStateChangedListener.onStateChanged(mState);
        }
    }

    public  void processState() {
        switch (mState) {
            case READY:
                mState = State.COINS_INPUT;
                break;
            case COINS_INPUT:
                mState = State.WORKING;
                break;
            case WORKING:
                mState= State.COFFEE;
                break;
            case COFFEE:
                mState = State.CHANGE;
                break;
            case CHANGE:
                mState = State.READY;
                break;
            case ERROR:
                // do nothing
                break;
            default:
                mState = State.ERROR;
                break;
        }

        if (null != mStateChangedListener) {
            mStateChangedListener.onStateChanged(mState);
        }
    }
}
