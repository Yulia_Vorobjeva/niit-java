package com.vorobjeva.apps.serviceapp;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;

public class ServiceAsyncQueryHandler extends AsyncQueryHandler {

    private UpdateDataBaseListener mUpdateDataBaseListener;

    public ServiceAsyncQueryHandler(ContentResolver cr, UpdateDataBaseListener updateDataBaseListener) {
        super(cr);
        mUpdateDataBaseListener = updateDataBaseListener;
    }

    @Override
    protected void onUpdateComplete(int token, Object cookie, int result) {
        super.onUpdateComplete(token, cookie, result);
        mUpdateDataBaseListener.onUpdateCompleted();
    }
}
