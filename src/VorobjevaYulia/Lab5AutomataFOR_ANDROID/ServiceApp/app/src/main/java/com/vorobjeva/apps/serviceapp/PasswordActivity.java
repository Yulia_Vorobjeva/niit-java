package com.vorobjeva.apps.serviceapp;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PasswordActivity extends Activity {

    private static final String PASSWORD = "123456";
    private EditText mPassEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        Button enterBtn = (Button) findViewById(R.id.enterBtn);
        Button clearBtn = (Button) findViewById(R.id.clearBtn);
        mPassEditText = (EditText) findViewById(R.id.passwordEditText);

        enterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = mPassEditText.getText().toString();
                if (PASSWORD.equals(text)) {
                    Intent intent = new Intent(PasswordActivity.this, ReloadActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(PasswordActivity.this, "Incorrect", Toast.LENGTH_LONG).show();
                    clearText();
                }

            }
        });

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearText();
            }
        });


        mPassEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString();
                if (text.length() > 6) {
                    mPassEditText.setText(text.substring(0, 6));
                }

            }
        });

    }

    private void clearText() {
        mPassEditText.setText("");
    }
}
