import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by yuliavorobjeva on 24.03.17.
 */
public class AutomataTest {
    @Test
    public void CheckON(){
        Automata automata = new Automata();
        automata.ON();
        assertEquals(automata.getState(), Automata.STATES.WAIT);
    }
    @Test
    public void CheckConstructor(){
        Automata automata = new Automata();
        assertEquals(automata.getCash(),0);
        assertEquals(automata.getHumanChoice(),0);
        assertEquals(automata.getHumanMoney(),0);
        assertEquals(automata.getState(), Automata.STATES.OFF);
        }

    @Test
    public void CheckOFF(){
        Automata automata = new Automata();
        automata.setState(Automata.STATES.COOK);
        automata.OFF();
        assertEquals(automata.getState(), Automata.STATES.COOK);
        automata.setState(Automata.STATES.WAIT);
        automata.OFF();
        assertEquals(automata.getState(), Automata.STATES.OFF);
    }

    @Test
    public void checkCoin(){
        Automata automata = new Automata();
        automata.ON();
        int x = 20;
        automata.coin(x);
        assertEquals(automata.getHumanMoney(),x);
        assertEquals(automata.getState(), Automata.STATES.ACCEPT);
        int y = -5;
        automata.setState(Automata.STATES.WAIT);
        automata.coin(y);
        assertEquals(automata.getHumanMoney(),x);
        assertEquals(automata.getState(), Automata.STATES.WAIT);
    }
    @Test
    public void checkChoise(){
        Automata automata = new Automata();
        automata.setState(Automata.STATES.ACCEPT);
        int x = 10;
        automata.choice(x);
        assertEquals(automata.getHumanChoice(),0);
        assertEquals(automata.getState(), Automata.STATES.ACCEPT);
        automata.setState(Automata.STATES.CHECK);
        x = 3;
        automata.choice(x);
        assertEquals(automata.getHumanChoice(),0);
        assertEquals(automata.getState(), Automata.STATES.CHECK);
        automata.setState(Automata.STATES.ACCEPT);
        automata.choice(x);
        assertEquals(automata.getHumanChoice(),3);
        assertEquals(automata.getState(), Automata.STATES.CHECK);
    }

    @Test
    public void checkCheck(){
        Automata automata = new Automata();
        automata.setState(Automata.STATES.WAIT);
        automata.check();
        assertEquals(automata.getState(), Automata.STATES.WAIT);
        automata.setState(Automata.STATES.CHECK);
        automata.setHumanMoney(10);
        automata.check();
        assertEquals(automata.getState(), Automata.STATES.CHECK);
        automata.setHumanMoney(50);
        automata.check();
        assertEquals(automata.getState(), Automata.STATES.COOK);
    }

    @Test
    public void checkCook(){
        Automata automata = new Automata();
        automata.setState(Automata.STATES.WAIT);
        automata.cook();
        assertEquals(automata.getState(), Automata.STATES.WAIT);
        automata.setState(Automata.STATES.COOK);
        automata.cook();
        assertEquals(automata.getState(), Automata.STATES.WAIT);
        assertEquals(automata.getHumanMoney(),0);
    }

    }

