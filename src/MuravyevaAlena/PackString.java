public class PackString {
   static final int MAX_SIZE = 80;
   static final int MIN_SIZE = 3;
   static char[] arrSymbol = new char[MAX_SIZE];
   static int[] arrInteger = new int[MAX_SIZE];
   static char[] arrNumber = new char[MIN_SIZE];
   static int arrSymbolIndex = 0;
   static int startPack=0;
   static int endPack=0;
   static int arrNumberIndex = 0;
   public static void main(String[] args) {
      
      PackString ps = new PackString();

      arrSymbol = args[0].toCharArray();
      for (arrSymbolIndex = 0;arrSymbolIndex<arrSymbol.length; arrSymbolIndex++) {
         ps.convertCharToInt();
      }

      int n=0;
      while(arrInteger[endPack+1] !=0){
         startPack= n;
         
         for(int m=n;arrInteger[m]!=0;m++){
            if( (arrInteger[m+1] - arrInteger[m]) > 1)
                  
            {
               endPack = m;
               break;
            }
            
            if(arrInteger[m+1]==0)
            {
               endPack = m;
            }
         }
         
         if(endPack - startPack > 1){
            System.out.printf("%d - %d,",arrInteger[startPack],arrInteger[endPack]);
         }
         else if(endPack - startPack == 1) {
            System.out.printf("%d, %d,",arrInteger[n], arrInteger[n+1]);
         }
         else{
            System.out.printf("%d,", arrInteger[n]);
         }
         n = endPack+1;
      }
   }
 
  void convertCharToInt(){
      int counterNum = 0;
      int arrNumIndex=0;
      int number = 0;
      while (arrSymbol[arrSymbolIndex] != ','&& arrSymbol[arrSymbolIndex]!='.') {
         if (arrSymbol[arrSymbolIndex] >= '0' && arrSymbol[arrSymbolIndex] <= '9') {
            arrNumber[arrNumIndex] = arrSymbol[arrSymbolIndex];
         }
         arrSymbolIndex++;
         arrNumIndex++;
         counterNum++;
      }
      
      for(int i=counterNum-1, j=1; i>=0; i--, j*=10){
         number += (Character.getNumericValue(arrNumber[i])) * j;
      }
      
      arrInteger[arrNumberIndex]=number;
      arrNumberIndex++;
   }
}
