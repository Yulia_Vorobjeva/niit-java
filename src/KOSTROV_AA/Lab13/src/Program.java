
import static java.lang.Integer.parseInt;

/**
 * Created by Alexander on 13.03.2017.
 */
public class Program {

    public static String go(String s){

        String itog="";
        String part ="";
        String[] split = s.split(",");
        char[] m = s.toCharArray();

        for (String sym : split){
            if (sym.indexOf('-') >= 0) {
                String[] splitPart = sym.split("-");
                for(int i=Integer.parseInt(splitPart[0]);i<=Integer.parseInt(splitPart[1]);i++)
                   itog+=i + ",";
            }
            else
                itog+=sym + ",";
        }
        itog = itog.substring(0, itog.length()-1);

        return itog;
    }
    public static void main(String[] args){
       System.out.println(go(args[0]));
    }
}
