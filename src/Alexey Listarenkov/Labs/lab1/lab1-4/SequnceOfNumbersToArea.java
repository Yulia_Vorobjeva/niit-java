/*
*Написать программу, которая принимает через командную строку числовую последовательность
*  и выдает на экран список чисел со свёрнутыми диапазонами. Например: 1,2,4,5,6,7,18,19,20,21 -> 1,2,4-7,18-21
 */


import java.util.ArrayList;

public class SequnceOfNumbersToArea {

    static String str1 = "0", str;
    static ArrayList<Integer> numbers = new ArrayList<>();
    
    public static void main(String[] args) {

        str = args[0];

        System.out.println();
        System.out.println("Входящие значения : " + str);
        System.out.println();

        fillArray();
        printAreas();

        System.out.println();
        System.out.println();

    }

    static void fillArray() {

        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {
                str1 = str1 + str.charAt(i);
            } else {
                numbers.add(Integer.parseInt(str1));
                str1 = "0";
            }
        }
        numbers.add(Integer.parseInt(str1));

        if (!Character.isDigit(str.charAt(str.length() - 1))) {
            numbers.remove(numbers.size() - 1);
        }
    }

     
     static void printAreas() {

        int count =0;

        System.out.print(numbers.get(0));
        if (numbers.size()==2) {
            System.out.print("," + numbers.get(1));
        }
        for (int i = 1; i < numbers.size()-1; i++) {
            if (numbers.get(i-1)+2 != numbers.get(i+1)){
                if (count > 0) {
                    System.out.print("-" + numbers.get(i));
                } else if (count == 0) {
                    System.out.print("," + numbers.get(i));
                }
            }
            count++;
            if (numbers.get(i-1)+2 != numbers.get(i+1)){
                count = 0;
            }
            if (i==numbers.size()-2) {
                if (numbers.get(i-1)+2 == numbers.get(i+1)){
                    System.out.print("-" + numbers.get(i+1));
                } else {
                    System.out.print("," + numbers.get(i+1));
                }
            }
        }      
    } 
} 


