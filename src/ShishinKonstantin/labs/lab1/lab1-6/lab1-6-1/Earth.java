
class Circle{
	private static double pi=3.14159265;
	private double radius=0.0, ference=0.0, area=0.0;	//инициируем переменные радиус, длина окружности и площадь
		
	double getFerence(double r)	//метод получения длины окружности с помощью радиуса
	{
		this.ference=2*pi*r;
		return this.ference;
	}
	double getArea(double r)	//метод получения площади с помощью радиуса
	{
		this.area=pi*r*r;
		return this.area;
	}
	double getRadiusFromFerence(double f)	//метод получения радиуса с помощью длины окружности
	{
		this.radius=f/(2*pi);
		return this.radius;
	}
	double getRadiusFromArea(double a)	//метод получения радиуса с помощью площади
	{
		this.radius=Math.sqrt(a/pi);
		return this.radius;
	}
}
public class Earth{
	public static void main(String[] args){
		Circle earth = new Circle();	//создаем объект Земля
		double radiusEarth = 6378100.0;	//радиус Земли в метрах
		double ferenceEarth = earth.getFerence(radiusEarth);	//получаем длину окружности Земли
		double ferenceRope = ferenceEarth+1;					//длина "окружности" веревки
		double radiusRope = earth.getRadiusFromFerence(ferenceRope);	//получаем радиус "окружности" веревки
		double spacing = radiusRope - radiusEarth;	//зазор
		System.out.format("spacing in meters = %.5f%n", spacing); //выводим зазор в метрах с точностью 5 знаков после ,
		
	}
}	