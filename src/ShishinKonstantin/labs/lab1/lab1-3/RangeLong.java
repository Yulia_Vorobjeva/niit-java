
public class RangeLong{
	public static void main(String[] args){
		
		String str = args[0];
		String[] arrStr = str.split(",");	//разбиваем полученную на входе строку на массив строк arrStr
		
		for(int i=0; i<arrStr.length; i++)
		{
			char[] arrChar = arrStr[i].toCharArray();	//приводим каждый элемент массива строк к массиву символов arrChar
			
			//перебираем элементы массива символов
			for(int j=0; j<arrStr[i].length(); j++)
			{
				if(arrChar[j]=='-')		//если в массиве символов встречается '-'
				{
					String[] rangeArr = arrStr[i].split("-");		//разбиваем строку на массив строк rangeArr, состоящий из двух элементов
					int rangeLeft = Integer.parseInt(rangeArr[0]);	//первый элемент массива строк rangeArr приводим к целому числу и приравниваем к левой границе диапазона (который нужно развернуть) 
					int rangeRight = Integer.parseInt(rangeArr[1]);	//второй элемент массива строк rangeArr приводим к целому числу и приравниваем к правой границе диапазона (который нужно развернуть)
					int rangeLength = rangeRight-rangeLeft;			//длина диапазона равна правая граница диапазона - левая граница диапазона
					arrStr[i] = "";		//делаем пустым текущий элемент массива строк arrStr
					arrStr[i] = arrStr[i]+Integer.toString(rangeLeft)+",";	//к текущему элементу массива строк arrStr прибавляем левую границу диапазона, приведенную к строке
					
					//перебираем элементы внутри диапазона
					for (int k=1; k<rangeLength; k++)
					{
						arrStr[i] = arrStr[i]+Integer.toString(rangeLeft+k)+","; 	//к текущему элементу массива строк arrStr прибавляем следующий элемент диапазона, приведенный к строке
						
					}
					arrStr[i] = arrStr[i]+Integer.toString(rangeRight);		//к текущему элементу массива строк arrStr прибавляем правую границу диапазона, приведенную к строке
					break;		//выходим из цикла
				}
			}
		if(i!=arrStr.length-1) System.out.print(arrStr[i]+",");		//если текущий элемент массива не последний, выводим его + ","
		else if(i==arrStr.length-1) System.out.print(arrStr[i]);		//если текущий элемент массива последний, выводим его без ","
		}
	}
}	

