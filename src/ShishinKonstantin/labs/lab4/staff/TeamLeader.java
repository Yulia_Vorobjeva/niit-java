package kostya;

//ведущий программист.
public class TeamLeader extends Programmer implements Heading{
    private int numSubordinates = 0;        //количество подчиненных у ведущего программиста

    public TeamLeader(int id, String name, String position, int base, int projectCost, int numLinesOfCodeInProject, int numWrittenLinesOfCode, int numSubordinates){
        super(id, name, position, base, projectCost, numLinesOfCodeInProject, numWrittenLinesOfCode);
        this.numSubordinates = numSubordinates;
    }

    //расчет оплаты исходя из руководства (количество подчиненных умножается на коэффициент "руководства")
    @Override
    public double calcPaymentForHeading() {
        return numSubordinates * 5000;
    }

    //расчет зароботной платы
    @Override
    public double calcPayment() {
        setPayment(calcPaymentForWorkTime() + calcPaymentForProject() + calcPaymentForHeading());
        return getPayment();
    }
}
