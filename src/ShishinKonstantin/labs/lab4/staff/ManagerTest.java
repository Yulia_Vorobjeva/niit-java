package kostya;

import junit.framework.TestCase;

/**
 * Created by kostya on 20.04.2017.
 */
public class ManagerTest extends TestCase {
    public void testCalcPayment() throws Exception {
        Manager manager = new Manager(1005,"Александрова А.А.","Manager",100000,1);
        assertEquals(10000.0,manager.calcPayment());
    }

}