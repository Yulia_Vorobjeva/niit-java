package kostya;

import junit.framework.TestCase;

/**
 * Created by kostya on 20.04.2017.
 */
public class SeniorManagerTest extends TestCase {
    public void testCalcPaymentForProject() throws Exception {
        SeniorManager seniorManager = new SeniorManager(1001,"Иванов И.И.","SeniorManager",0,1000000,0,30);
        assertEquals(300000.0,seniorManager.calcPaymentForProject());
    }

    public void testCalcPaymentForHeading() throws Exception {
        SeniorManager seniorManager = new SeniorManager(1001,"Иванов И.И.","SeniorManager",0,1000000,0,30);
        assertEquals(450000.0,seniorManager.calcPaymentForHeading());
    }

    public void testCalcPayment() throws Exception {
        SeniorManager seniorManager = new SeniorManager(1001,"Иванов И.И.","SeniorManager",0,1000000,0,30);
        assertEquals(750000.0,seniorManager.calcPayment());
    }

}