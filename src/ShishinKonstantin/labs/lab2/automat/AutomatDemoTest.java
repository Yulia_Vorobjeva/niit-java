package kostya;

/*import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;*/

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
public class AutomatDemoTest 
    //extends TestCase
{
    @Test
    public void testOn() throws Exception {
        Automat testAutomat = new Automat();
        Automat.STATES state = testAutomat.on();
        assertEquals(Automat.STATES.WAIT, state);
    }

    @Test
    public void testOff() throws Exception {
        Automat testAutomat = new Automat();
        Automat.STATES state = testAutomat.off();
        assertEquals(Automat.STATES.OFF, state);
    }

    @Test
    public void testPrintState() throws Exception {
        Automat testAutomat = new Automat();
        testAutomat.on();
        String result = testAutomat.printState();
        assertEquals("STATE: WAIT", result);
    }

    @Test
    public void testPrintMenu() throws Exception {
        Automat testAutomat = new Automat();
        testAutomat.on();
        String[] result = testAutomat.printMenu();
        assertEquals("1: Tea - 25 rub.", result[0]);
    }

    @Test
    public void testCoin() throws Exception {
        Automat testAutomat = new Automat();
        testAutomat.on();
        testAutomat.coin(20);
        int result = testAutomat.coin(20);
        assertEquals(40, result);
    }

    @Test
    public void testCancel() throws Exception {
        Automat testAutomat = new Automat();
        testAutomat.on();
        testAutomat.coin(20);
        String result = testAutomat.cancel();
        assertEquals("Take the money!", result);
    }

    @Test
    public void testChoice() throws Exception {
        Automat testAutomat = new Automat();
        testAutomat.on();
        testAutomat.coin(20);
        String result = testAutomat.choice(2);
        assertEquals("Coffee", result);
    }

    @Test
    public void testCheck() throws Exception {
        Automat testAutomat = new Automat();
        testAutomat.on();
        testAutomat.coin(50);
        testAutomat.choice(2);
        String result = testAutomat.strReturnCheck;
        assertEquals("Your change: 20 rub.", result);
    }

    @Test
    public void testCook() throws Exception {
        Automat testAutomat = new Automat();
        Automat.STATES state = testAutomat.on();
        assertEquals(Automat.STATES.WAIT, state);
    }

    @Test
    public void testFinish() throws Exception {
        Automat testAutomat = new Automat();
        testAutomat.on();
        testAutomat.coin(50);
        testAutomat.choice(2);
        String result = testAutomat.strReturnFinish;
        assertEquals("Coffee is ready!", result);
    }

}
