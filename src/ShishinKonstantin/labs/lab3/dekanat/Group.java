package kostya;

//класс Группа
public class Group
{
	private String title;										//название группы
	private Student[] studentsInGroup = new Student[30];		//массив из ссылок на студентов
	private int numStudentsInGroup=0;							//количество студентов в группе
	private Student head;										//ссылка на старосту (из членов группы)

	//создание группы с указанием названия
	public Group(String title) {
		this.title=title;
	}

	//получение названия группы
	public String getTitle() {
		return title;
	}

	//добавление студента в группу
	public void addStudentInGroup(Student student) throws ArrayIndexOutOfBoundsException{
		studentsInGroup[numStudentsInGroup]=student;
		numStudentsInGroup++;
	}

	//исключение студента из группы
	public void deleteStudentFromGroup(Student student) {
		Student[] studentsInGroupAfterDelete = new Student[numStudentsInGroup-1];	//создание временного массива
		int num=0;	//количество неисключенных студентов
		for(int i=0; i<numStudentsInGroup; i++){
			if(!(studentsInGroup[i].equals(student))){
				studentsInGroupAfterDelete[num]=studentsInGroup[i];	//копирование неисключенных студентов во временный массив
				num++;
			}
		}
		numStudentsInGroup--;
		for(int i=0; i<numStudentsInGroup; i++) {
			studentsInGroup[i]=studentsInGroupAfterDelete[i];	//копирование неисключенных студентов в массив студентов
		}
	}

	//избрание старостой группы студента с максимальным средним баллом
	public void choiceHead() {
		double maxAverageMarkStudent=0;
		for(int i=0; i<numStudentsInGroup; i++){
			if(studentsInGroup[i].calcAverageMarkStudent()>maxAverageMarkStudent){
				maxAverageMarkStudent=studentsInGroup[i].calcAverageMarkStudent();
				head=studentsInGroup[i];
			}
		}
		System.out.println("Староста группы " + head.getGroup().getTitle() + " - " + head.getFio());
	}

	//вычисление среднего балла в группе
	public double calcAverageMarkGroup() {
		double sumMarksGroup = 0.0;
		for (int i = 0; i < numStudentsInGroup; i++){
			sumMarksGroup+=studentsInGroup[i].calcAverageMarkStudent();
		}
		return sumMarksGroup/numStudentsInGroup;
	}
}
