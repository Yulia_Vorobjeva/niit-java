public class Automat{
    enum STATES {OFF, WAIT, ACCEPT, CHECK, COOK}	//выключено, ожидание, прием денег, проверка наличности, приготовление
    private STATES state;	//текущее состояние
    private int cash = 0;	//текущая сумма денег в рублях
    private String[] menu = {"ЧАЙ","ЧЕРНЫЙ КОФЕ","КАПУЧИНО","ГОРЯЧИЙ ШОКОЛАД","МОЛОЧНЫЙ НАПИТОК","КИПЯТОК"};		//массив названий напитков
    private int[] prices = {25,35,45,40,15,10};	//массив цен напитков
    private int numDrink = 0;	//номер напитка в списке

    private String strCoin = "";    //строка, получаемая в результате выполнения метода coin() (необходима для формы)
    private String strCheck = "";   //строка, получаемая в результате выполнения метода check() (необходима для формы)
    private String strFinish = "";  //строка, получаемая в результате выполнения метода finish() (необходима для формы)

    //конструктор автомата
    Automat() {
        state = STATES.OFF;
    }

    //возвращение текущей суммы
    public int getCash(){
        return cash;
    }

    //возвращение текущего состояния
    public STATES getState(){
        return state;
    }

    //возвращение массива цен (для добавления на кнопки)
    public int[] getPrices(){
        return prices;
    }

    //возвращение строки, получаемой в результате выполнения метода coin()
    public String getStrCoin(){
        return strCoin;
    }

    //возвращение строки, получаемой в результате выполнения метода check()
    public String getStrCheck(){
        return strCheck;
    }

    //возвращение строки, получаемой в результате выполнения метода finish()
    public String getStrFinish(){
        return strFinish;
    }

    //включение
    public STATES on() {
        if(state == STATES.OFF) state = STATES.WAIT;
        return state;
    }

    //выключение
    public STATES off() {
        if(state == STATES.WAIT) state = STATES.OFF;
        return state;
    }

    //занесение денег пользователем
    public int coin(int cash) {
        if(state == STATES.WAIT || state == STATES.ACCEPT) {
            state = STATES.ACCEPT;
            this.cash += cash;
            strCoin = "Внесение денег...";
        }
        return this.cash;
    }

    //отмена сеанса обслуживания пользователем
    public STATES cancel() {
        if(state == STATES.ACCEPT) {
            state = STATES.WAIT;
            cash = 0;
        }
        return state;
    }

    //выбор напитка пользователем
    public String choice(int numDrink) {
        if(state == STATES.ACCEPT || state == STATES.WAIT) {
            state = STATES.CHECK;
            this.numDrink=numDrink-1;
            check();
        }
        return menu[this.numDrink];
    }

    //проверка наличия необходимой суммы
    private void check() {
        strCheck = "";
        if(state==STATES.CHECK) {
            if(cash<prices[numDrink]){
                strCheck = "Недостаточная сумма денег!";
                state = STATES.ACCEPT;
            }
            else if(cash>prices[numDrink]) {
                strCheck = "Ваша сдача: " + (cash - prices[numDrink]) + " р.";
                cash = 0;
                state = STATES.COOK;
            }
            else {
                strCheck = "Подождите...";
                cash = 0;
                state = STATES.COOK;
            }
        }
    }

    //завершение обслуживания пользователя
    protected void finish() {
        strFinish = "";
        if(state==STATES.COOK) {
            strFinish = "Ваш " + menu[this.numDrink]+ " готов!";
            state = STATES.WAIT;
        }
    }
}


