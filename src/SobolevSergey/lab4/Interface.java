package staff;

// WorkTime - расчет оплаты исходя из отработанного времени. получает часы, ставку, возвращает сумму
interface WorkTime{
    int workTime(int hour, int base);
}

// Project - расчет оплаты исходя из участия в проекте.
interface Project{
    double project(String project, int projectCost, double degreeParticipation);
}

// Heading - расчет оплаты исходя из руководства.
interface Heading {
     int heading(int numberSubordinates,int rateSubordinate);
}

