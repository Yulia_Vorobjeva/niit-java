package staff;

// Manager - менеджер. Оплату получает из денег проекта, которым руководит.
public class  Manager extends Employee implements Project{

    protected String project;
    protected int projectCost;
    protected double degreeParticipation;

    public Manager(int id, String name, String project, int projectCost, double degreeParticipation) {

        super(id, name);

        this.project = project;
        this.projectCost = projectCost;
        this.degreeParticipation = degreeParticipation;
    }

    public double project (String project, int projectCost, double degreeParticipation) {
            return projectCost * degreeParticipation;
    }

    public void calc(){
        setPayment((int) project (project, projectCost, degreeParticipation));
    }
}

// ProjectManager - проектный менеджер.
class ProjectManager extends Manager implements Heading{
    protected int numberSubordinates;
    protected int rateSubordinate;

    public ProjectManager(int id, String name, String project, int projectCost, double degreeParticipation,
                          int numberSubordinates, int rateSubordinate) {

        super(id, name, project, projectCost, degreeParticipation);

        this.numberSubordinates = numberSubordinates;
        this.rateSubordinate = rateSubordinate;
    }

    public int heading (int numberSubordinates,int rateSubordinate) {
        return numberSubordinates*rateSubordinate;
    }

    public void calc(){
        setPayment((int) (project (project, projectCost, degreeParticipation)+heading (numberSubordinates,rateSubordinate)));
    }
}

// SeniorManager - руководитель направления.
final class SeniorManager extends ProjectManager{
    public SeniorManager(int id, String name, String project, int projectCost, double degreeParticipation,
                            int numberSubordinates, int rateSubordinate) {

        super(id, name, project, projectCost, degreeParticipation, numberSubordinates, rateSubordinate);
    }
}
