package Dekanat;
/**
 * Created by SKS on 02.04.2017.
 */
/*Разработать класс Student для хранения информации о студенте.
Примерный перечень полей:
ID - идентификационный номер
Fio - фамилия и инициалы
Group - ссылка на группу (объект Group)
Marks - массив оценок
Num - количество оценок

Обеспечить класс следующими методами:
создание студента с указанием ИД и ФИО
зачисление в группу
добавление оценки
вычисление средней оценки
*/

public class Student {

    private int ID;                                         //ID - идентификационный номер
    private String Fio;                                     //Fio - фамилия и инициалы
    private Group group;                                    //Group - ссылка на группу (объект Group)
    protected int[] Marks = new int[10];                    //Marks - массив оценок
    private int Num = 0;                                    //Num - количество оценок


    //Обеспечить класс следующими методами:
    //создание студента с указанием ИД и ФИО
    Student(int ID, String Fio){
        this.ID = ID;
        this.Fio = Fio;
        for (int i = 0; i < Marks.length; i++)
            Marks[i] = 0;
        this.Num = 0;
    }

    //зачисление в группу
    public void setGroup(Group group) {
        this.group = group;
    }

    //добавление оценки
    public void addMark(int mark) throws ArrayIndexOutOfBoundsException {
        Marks[Num++]=mark;
    }
    public void setMarks(int[] marks) throws ArrayIndexOutOfBoundsException {
        Marks = marks;
    }
    public int[] getMarks() {
        return Marks;
    }

    //вычисление средней оценки студента
    public double CalculatingAverageRatingStudent(){
        double temp = 0.0;
        for (int i=0; i<Num; i++){
            temp +=(double)Marks[i];
        }
        return (double)temp/Num;
    }

    public int getID() {
        return ID;
    }
    public void setID(int ID) {
        this.ID = ID;
    }

    public String getFio() {
        return Fio;
    }
    public void setFio(String Fio) {
        this.Fio = Fio;
    }

    public Group getGroup(){
        return group;
    }

    public int getNum() {
        return Num;
    }

    public void setNum(int num) {
        Num = num;
    }

}
