package Automata;

//import junit.framework.Test;---------------------------------
//import junit.framework.TestCase;-----------------------------
//import junit.framework.TestSuite;------------------------------------

//import org.junit.Test;
import org.junit.*;
//import org.junit.Assert;
import static Automata.Automata.STATES.ACCEPT;
import static Automata.Automata.STATES.OFF;
import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
public class AppTest 
//    extends TestCase--------------------------------------------
{
   /*
    public void testAverege() throws Exception {
        int result=App.average(3,5);
        assertEquals(result,4);

    }
    */

   @Test
    public void testMain() throws Exception {

    }

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */

    //@Test
    //public AppTest( String testName )
    //{
    //    super( testName );
    //}

    /**
     * @return the suite of tests being tested
     */

    //@Test
    //public static Test suite()
    //{
        //return new TestSuite( AppTest.class );
    //}

    /**
     * Rigourous Test :-)
     */

    @Test
    public void testApp()
    {

        assertTrue( true );
    }

    @Test
    public void testCoin()
    {
        Automata automata = new Automata();
        int count = automata.coin(10);
        assertEquals(automata.cash, 10);
    }

    @Test
    public void testPrintMenu()
    {
        Automata automata = new Automata();
        int count = automata.coin(10);
        assertSame(automata.state, ACCEPT);
        automata.printMenu();
    }

    @Test
    public void cancelSession()
    {
        Automata automata = new Automata();
        int count = automata.coin(10);
        int cancelSession = 2;
        assertSame(automata.state, ACCEPT);
    }

    @Test
    public void choice()
    {
        Automata automata = new Automata();
        int count = automata.coin(10);
        int indexItem = 1;
        automata.choice(indexItem);
        assertSame(automata.menuItem, 1);

    }

}
