package mypackage;

/**
 * Created by BalandinAS on 26.04.2017.
 */
public class SeniorManager extends ProjectManager
{
    final static int paymentForEveryProject = 500;
    public SeniorManager(int id, String name)
    {
        super(id, name);
    }

    @Override
    public int calcPayment()
    {
        return super.calcPayment() + paymentForEveryProject;
    }
}