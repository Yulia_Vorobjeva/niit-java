import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;

/**
 * Created by BalandinAS on 28.04.2017.
 */
public class AutomataGUI extends JFrame
{
    private Automata automata;
    private JPanel mainPanel;

    private JTabbedPane mainTabbedPane;

    private JPanel userPanel;
    private JButton addCoinButton;
    private JButton chooseButton;
    private JButton cancelButton;
    private JTextArea infoTextArea;
    private JList menuList;
    private JTextField coinTextField;
    private JProgressBar cookingProgressBar;

    private JPanel adminPanel;
    private JButton openMenuFileButton;
    private JButton onButton;
    private JTextArea infoForAdmin;

    public AutomataGUI()
    {
        //super();
        setContentPane(mainPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 600);
        setVisible(true);

        onButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                if (automata.getState().equals(Automata.STATES.OFF.toString()))
                {
                    //on automat
                    onButton.setText("Off");
                    mainTabbedPane.setEnabledAt(1, true);
                    mainTabbedPane.setSelectedComponent(userPanel);
                    infoTextArea.setText("Hello! I'm an automat! Insert coins..." + Arrays.toString(automata.getNominalsOfCoins()));
                    automata.on();
                } else if (automata.getState().equals(Automata.STATES.WAIT.toString()))
                {
                    //off automat
                    onButton.setText("On");
                    mainTabbedPane.setEnabledAt(1, false);
                    automata.off();
                } else
                {
                    JOptionPane.showMessageDialog(null, "Wrong state!");
                }
                updateInfoForAdmin();
            }
        });

        openMenuFileButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                final JFileChooser fileChooser = new JFileChooser();
                fileChooser.showOpenDialog(AutomataGUI.this);
                File f = fileChooser.getSelectedFile();
                try
                {
                    InitAutomataFile initAutomataFile = new InitAutomataFile(f);
                    initAutomata(initAutomataFile);
                    //JOptionPane.showMessageDialog(null, "Information from " + f.getAbsolutePath() + "successfully loaded!");
                    onButton.setEnabled(true);
                    initMenuList();
                    updateInfoForAdmin();
                } catch (Exception e1)
                {
                    JOptionPane.showMessageDialog(null, "File: " + f.getAbsolutePath() + "is wrong!");
                }
            }
        });

        addCoinButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e) //throws AutomataException
            {
                try
                {
                    if (!automata.coin(Integer.parseInt(coinTextField.getText())))
                        JOptionPane.showMessageDialog(null, "You inserted a faked coin!");
                    else
                    {
                        coinTextField.setText("0");
                        infoTextArea.setText("You inserted: " + automata.getSumOfMoneyBuf() + "$");
                    }
                    updateInfoForAdmin();
                } catch (AutomataException ae)
                {
                    new AutomataException("Wrong state!");
                } catch (NumberFormatException ne)
                {
                    new NullPointerException("Wrong coins!");
                }
            }
        });

        chooseButton.addActionListener(new ActionListener()
        {
            Timer timer;
            int i;
            int choice;
            int changeCoins[];
            public void actionPerformed(ActionEvent e)
            {
                changeCoins = new int[automata.getNominalsOfCoins().length];
                if((choice = menuList.getSelectedIndex() + 1) == 0)
                    return;
                if (automata.check(choice = automata.choice(choice)))
                {
                    //Create a timer.
                    automata.cook();
                    timer = new Timer(20, new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            if (i >= 100)
                            {
                                //JOptionPane.showMessageDialog(null, "Take your " + automata.getMenu()[choice] + "!");
                                changeCoins = automata.change();
                                if(sumArray(changeCoins) != 0)
                                    JOptionPane.showMessageDialog(null, "Take your " + automata.getMenu()[choice] + "!\n" +
                                            buildChangeCancelString(changeCoins)
                                    );
                                else
                                    JOptionPane.showMessageDialog(null, "Take your " + automata.getMenu()[choice]);
                                infoTextArea.setText("Hello! I'm an automat! Insert coins..." + Arrays.toString(automata.getNominalsOfCoins()));
                                i = 0;
                                updateInfoForAdmin();
                                timer.stop();
                            }
                            else
                            {
                                infoTextArea.setText("Cooking...");
                                i++;
                            }
                            cookingProgressBar.setValue(i);
                            cookingProgressBar.setString("Cooking " + i + " %");
                            menuList.clearSelection();
                        }
                    });
                    timer.start();
                    updateInfoForAdmin();
                }
            }
        });

        cancelButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                JOptionPane.showMessageDialog(null, "Take your money: " + sumArray(automata.cancel()) + "$");
                infoTextArea.setText("Hello! I'm an automat! Insert coins..." + Arrays.toString(automata.getNominalsOfCoins()));
                updateInfoForAdmin();
            }
        });
    }

    private void initAutomata(InitAutomataFile initAutomataFile)
    {
        automata = new Automata(initAutomataFile.getMenu(), initAutomataFile.getPrices(), initAutomataFile.getMoneyCash(), initAutomataFile.getCoins());
    }

    private int sumArray(int[] arr)
    {
        int sum = 0;
        for(int i : arr)
            sum += i;
        return sum;
    }



    private String buildChangeCancelString(int[] arr)
    {
        String str = "Take your change:\n(sum : coins)\n";
        for(int i =0; i < arr.length; i++)
            str += arr[i] + "$ : " + automata.getNominalsOfCoins()[i] + "$\n";
        return str;
    }

    private void initMenuList()
    {
        DefaultListModel menuDefaultList = new DefaultListModel();
        for(String s : automata.getWholeMenu())
            menuDefaultList.addElement(s);
        menuList.setModel(menuDefaultList);
    }

    private void updateInfoForAdmin()
    {
        infoForAdmin.setText("State: " + automata.getState() + "\n" +
                "moneyBuf :" + automata.getSumOfMoneyBuf() + "\n" +
                "coins :" + Arrays.toString(automata.getNominalsOfCoins()) + "\n" +
                "cash  :" + Arrays.toString(automata.getMoneyCash()) + "\n" +
                "menu:"  + "\n" +
                Arrays.toString(automata.getWholeMenu())
        );
    }

    public static void main(String args[])
    {
        new AutomataGUI();
    }
}