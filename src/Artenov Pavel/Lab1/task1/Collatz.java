public class Collatz {
    public static int collatz(long i,int step) {
	    step++;
	    if (i == 1) return step;
	    else if (i % 2 == 0)  return collatz(i/ 2,step);
	    else  return collatz(3*i + 1,step);
    }
    public static void main(String [] args) {
	    int i = 2,MAX_collatz=0;
	    int m,MAX_step=0;
	    while(i<=1000000){
		    m=collatz(i,0);
		    if(m>MAX_step) {
			    MAX_step=m;
			    MAX_collatz=i;
		    }
		    i++;
	    }
	    System.out.println("Максимальное кол-во шагов "+MAX_step + " при "+ MAX_collatz);
    }
}